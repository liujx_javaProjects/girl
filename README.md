# girl

#### 介绍
springboot第二个入门项目

#### 软件架构
[廖师兄的第二门springboot课程](https://www.imooc.com/learn/810)
主要内容：

- bindingResult验证
- aop统一日志处理
- 简单封装了通用返回对象
- 使用了enum枚举类
- 异常处理

